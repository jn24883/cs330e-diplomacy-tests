#!/usr/bin/env python3

# ----------------
# imports
# ----------------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, Army

# ----------------
# Test Diplomacy
# ----------------


class TestCollatz (TestCase):
    # ---------
    # read
    # ---------

    def test_read_1(self):
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        arr = diplomacy_read(s)
        a = ["A", "Madrid", ["Hold"]]
        b = ["B", "Barcelona", ["Move", "Madrid"]]
        self.assertEqual([arr[0].name, arr[0].location, arr[0].action], a)
        self.assertEqual([arr[1].name, arr[1].location, arr[1].action], b)

    def test_read_2(self):
        s = StringIO(
            "A Madrid Hold\nB London Support A\nC Austin Move London\nD Beijing Move Madrid\nE Dallas Support B")
        arr = diplomacy_read(s)
        a = ["A", "Madrid", ["Hold"]]
        b = ["B", "London", ["Support", "A"]]
        c = ["C", "Austin", ["Move", "London"]]
        d = ["D", "Beijing", ["Move", "Madrid"]]
        e = ["E", "Dallas", ["Support", "B"]]

        self.assertEqual([arr[0].name, arr[0].location, arr[0].action], a)
        self.assertEqual([arr[1].name, arr[1].location, arr[1].action], b)
        self.assertEqual([arr[2].name, arr[2].location, arr[2].action], c)
        self.assertEqual([arr[3].name, arr[3].location, arr[3].action], d)
        self.assertEqual([arr[4].name, arr[4].location, arr[4].action], e)

    def test_read_3(self):
        s = StringIO(
            "A Austin Move Houston\nB Houston Move Austin\nC Sydney Hold\n")
        arr = diplomacy_read(s)
        a = ["A", "Austin", ["Move", "Houston"]]
        b = ["B", "Houston", ["Move", "Austin"]]
        c = ["C", "Sydney", ["Hold"]]
        self.assertEqual([arr[0].name, arr[0].location, arr[0].action], a)
        self.assertEqual([arr[1].name, arr[1].location, arr[1].action], b)
        self.assertEqual([arr[2].name, arr[2].location, arr[2].action], c)

    # ---------
    # eval
    # ---------

    def test_eval_1(self):
        armies = []
        b = Army("B", "Barcelona", ["Move", "Madrid"])
        a = Army("A", "Madrid", ["Hold"])
        armies.append(b)
        armies.append(a)
        results = diplomacy_eval(armies)
        self.assertEqual(results[0].result, "[dead]")
        self.assertEqual(results[1].result, "[dead]")

    def test_eval_2(self):
        armies = []
        a = Army("A", "Madrid", ["Hold"])
        b = Army("B", "London", ["Support", "A"])
        c = Army("C", "Austin", ["Move", "London"])
        d = Army("D", "Beijing", ["Move", "Madrid"])
        e = Army("E", "Dallas", ["Support", "B"])
        armies.append(a)
        armies.append(b)
        armies.append(c)
        armies.append(d)
        armies.append(e)
        results = diplomacy_eval(armies)
        self.assertEqual(results[0].result, "[dead]")
        self.assertEqual(results[1].result, "London")
        self.assertEqual(results[2].result, "[dead]")
        self.assertEqual(results[3].result, "[dead]")
        self.assertEqual(results[4].result, "Dallas")

    def test_eval_3(self):
        armies = []
        a = Army("A", "Madrid", ["Hold"])
        b = Army("B", "Barcelona", ["Support", "A"])
        c = Army("C", "Paris", ["Support", "A"])
        d = Army("D", "SouthAfrica", ["Support", "E"])
        e = Army("E", "NewYork", ["Move", "Madrid"])
        armies.append(a)
        armies.append(b)
        armies.append(c)
        armies.append(d)
        armies.append(e)
        results = diplomacy_eval(armies)
        self.assertEqual(results[0].result, "Madrid")
        self.assertEqual(results[1].result, "Barcelona")
        self.assertEqual(results[2].result, "Paris")
        self.assertEqual(results[3].result, "SouthAfrica")
        self.assertEqual(results[4].result, "[dead]")

    # ---------
    # print
    # ---------

    def test_print_1(self):
        w = StringIO()
        armies = []
        armies.append(Army("A", "Madrid", ["Hold"]))
        armies.append(Army("B", "Barcelona", ["Move", "Madrid"]))
        armies[0].result = "[dead]"
        armies[1].result = "[dead]"
        diplomacy_print(armies, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_print_2(self):
        w = StringIO()
        armies = []
        armies.append(Army("A", "Madrid", ["Hold"]))
        armies.append(Army("B", "London", ["Support", "A"]))
        armies.append(Army("C", "Austin", ["Move", "London"]))
        armies.append(Army("D", "Beijing", ["Move", "Madrid"]))
        armies.append(Army("E", "Dallas", ["Support", "B"]))
        armies[0].result = "[dead]"
        armies[1].result = "[London]"
        armies[2].result = "[dead]"
        armies[3].result = "[dead]"
        armies[4].result = "[Dallas]"
        diplomacy_print(armies, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [London]\nC [dead]\nD [dead]\nE [Dallas]\n")

    def test_print_3(self):
        w = StringIO()
        armies = []
        armies.append(Army("A", "Austin", ["Move", "Houston"]))
        armies.append(Army("B", "Houston", ["Move", "Austin"]))
        armies.append(Army("C", "Sydney", ["Hold"]))
        armies[0].result = "[Houston]"
        armies[1].result = "[Austin]"
        armies[2].result = "[Sydney]"
        diplomacy_print(armies, w)
        self.assertEqual(w.getvalue(), "A [Houston]\nB [Austin]\nC [Sydney]\n")

    # ---------
    # solve
    # ---------

    def test_solve_1(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_2(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_3(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")


# -------
# main
# -------
if __name__ == "__main__":  # pragma: no cover
    main()
