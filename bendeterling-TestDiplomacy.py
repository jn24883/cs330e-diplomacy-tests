#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    
    def test_1(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")
        
    def test_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A dead\nB Madrid\nC London\n")
        
    def test_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A dead\nB dead\n")

    def test_4(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")

    def test_5(self):
        r = StringIO("A Madrid Hold\nB London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB London\n")

    def test_6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Stay Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC London Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\n")
    
    def test_8(self):
        # if A supports B and B supports C, just have A support C
        r = StringIO("A Madrid Support B\nB Barcelona Support C\nC London Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\n")

    def test_9(self):
        r = StringIO("A Madrid Move Beijing\nB Barcelona Support A\nC London Move Beijing\nD Houston Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A dead\nB Barcelona\nC dead\nD Houston\n")

    def test_10(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC London Hold\nD Houston Support C\nE Beijing Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\nD Houston\nE Beijing\n")

    def test_11(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB dead\nC London\n")



# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
